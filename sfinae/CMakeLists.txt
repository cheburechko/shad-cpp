cmake_minimum_required(VERSION 2.8)
project(sfinae)

if (TEST_SOLUTION)
  include_directories(../private/sfinae)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)

endif()

set(CXX_STANDARD 14)

add_library(gmock ${CMAKE_CURRENT_SOURCE_DIR}/../contrib/gmock-gtest-all.cc)
add_executable(test_advance
  ${CMAKE_CURRENT_SOURCE_DIR}/../contrib/gmock_main.cc
  test_advance.cpp)
add_executable(test_set
  ${CMAKE_CURRENT_SOURCE_DIR}/../contrib/gmock_main.cc
  test_set.cpp)
target_link_libraries(test_advance gmock pthread dl)
target_link_libraries(test_set gmock pthread dl)
